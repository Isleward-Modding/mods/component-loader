# Deprecated
There's now an official way to do this and this mod still uses requirejs.


# Component Loader
A simple mod for loading additional components into Isleward from inside another mod.

## Usage
Add the `component-loader` mod to your server's mods directory.

Then, in your mod that adds a new component, do something like this:
```js
define([
	'mods/component-loader/index'
], function(
	loader
) {
	return {
		name: '<your new mod>',

		extraScripts: [
			// any extra scripts you need
		],

		init: function () {
			// the line that does the magic
			loader.load(this.component);
		},
		
		component: {
			type: '<required component name>'
			
			// ... other component code stuff ...
		}
	};
});
```
